﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(SoundBank.ClipDictionary))]
[CustomPropertyDrawer(typeof(MaterialBank.MaterialDictionary))]
public class ClipDictionaryDrawer : SerializableDictionaryPropertyDrawer { }
