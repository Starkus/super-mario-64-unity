using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VirtualPad : MonoBehaviour {

	public Dictionary<string, VirtualButton> buttons { get; private set; }

	// Use this for initialization
	void Start () {

		if (!Application.isMobilePlatform)
		{
			Destroy(gameObject);
			return;
		}

		GetComponent<Canvas>().enabled = true;

		buttons = new Dictionary<string, VirtualButton>();

		for (int i=0; i < transform.childCount; ++i)
		{
			Transform child = transform.GetChild(i);

			VirtualButton b = child.GetComponent<VirtualButton>();
			if (b != null)
			{
				buttons.Add(child.gameObject.name, b);
			}
		}
	}

	public bool GetButtonDown(string b)
	{
		if (buttons.ContainsKey(b))
			return buttons[b].down;

		return false;
	}

	public bool GetButtonUp(string b)
	{
		if (buttons.ContainsKey(b))
			return buttons[b].up;

		return false;
	}

	public bool GetButton(string b)
	{
		if (buttons.ContainsKey(b))
			return buttons[b].held;

		return false;
	}
}
