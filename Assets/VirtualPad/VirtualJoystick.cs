using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class VirtualJoystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler {

	public float deadzone = 0.2f;

	private Image stickBackground, stick;
	private Vector3 inputVector;

	public float horizontal
	{
		get
		{
			if (inputVector.x != 0)
				return inputVector.x;
			else
				return Input.GetAxis("Horizontal");
		}
	}

	public float vertical
	{
		get
		{
			if (inputVector.z != 0)
				return inputVector.z;
			else
				return Input.GetAxis("Vertical");
		}
	}


	// Use this for initialization
	void Start () {
		stickBackground = GetComponent<Image>();
		stick = transform.GetChild(0).GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnDrag(PointerEventData eventData)
	{
		Vector2 pos;
		if (RectTransformUtility.ScreenPointToLocalPointInRectangle(stickBackground.rectTransform,
								eventData.position,
								eventData.pressEventCamera,
								out pos))
		{
			pos.x = (pos.x / stickBackground.rectTransform.sizeDelta.x);
			pos.y = (pos.y / stickBackground.rectTransform.sizeDelta.y);

			inputVector = new Vector3(pos.x * 2 - 1, 0, pos.y * 2 - 1);
			inputVector = (inputVector.magnitude > 1) ? inputVector.normalized : inputVector;

			if (inputVector.magnitude > deadzone)
				inputVector = inputVector.normalized * (inputVector.magnitude - deadzone);
			else
				inputVector = Vector3.zero;

			stick.rectTransform.anchoredPosition = new Vector3(inputVector.x * (stickBackground.rectTransform.sizeDelta.x / 2),
															inputVector.z * (stickBackground.rectTransform.sizeDelta.y / 2));
		}
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		inputVector = Vector3.zero;
		stick.rectTransform.anchoredPosition = inputVector;
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		OnDrag(eventData);
	}
}
