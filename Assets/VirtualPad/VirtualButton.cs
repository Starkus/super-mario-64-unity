using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class VirtualButton : Button {

	bool _up, _down, _held, _lastheld;

	public bool down { get { return _down; } }
	public bool up { get { return _up; } }
	public bool held { get { return _held; } }

	private float tapTimer = -1f;

	// Update is called once per frame
	void Update () {

		_up		= !_held &&  _lastheld;
		_down	=  _held && !_lastheld;

		_lastheld = _held;

		if (tapTimer > 0)
		{
			tapTimer -= Time.deltaTime;

			if (tapTimer < 0)
				_held = false;
		}
	}

	public override void OnPointerDown(PointerEventData eventData)
	{
		base.OnPointerDown(eventData);

		_held = true;
	}

	public override void OnPointerUp(PointerEventData eventData)
	{
		base.OnPointerUp(eventData);

		_held = false;
	}

	public void Tap()
	{
		_held = true;
		tapTimer = 0.1f;
	}
}
