﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OctogonalCamera : MonoBehaviour
{
	Transform mario;

	readonly float cThreshold = 0.3f;

	float cooldown = 0;
	bool cLeft, cRight;
	bool cLeftPrev = false;
	bool cRightPrev = false;

    // Start is called before the first frame update
    void Start()
    {
        mario = GameObject.FindWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
		cooldown -= Time.deltaTime;

		if (Input.GetAxis("CHorizontal") > cThreshold)
			cRight = !cRightPrev;
		else
			cRight = false;

		if (Input.GetAxis("CHorizontal") < -cThreshold)
			cLeft = !cLeftPrev;
		else
			cLeft = false;

        transform.position = mario.position;

		if (cooldown <= 0) {
			if (cRight) {
				transform.Rotate(0, -45f, 0);
				cooldown = 0.066f;
			}
			else if (cLeft) {
				transform.Rotate(0, 45f, 0);
				cooldown = 0.066f;
			}
		}

		cLeftPrev = Input.GetAxis("CHorizontal") < -cThreshold;
		cRightPrev = Input.GetAxis("CHorizontal") > cThreshold;
    }
}
