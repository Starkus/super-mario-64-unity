﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mario.States;

namespace Mario {
	public class RayCaster : MonoBehaviour {

		private readonly float SQRT2 = 1.4142f;

		private Controller _controller;
		private Controller C {
			get {
				if (_controller == null)
					_controller = GetComponent<Controller>();
				return _controller;
			}
		}

		public readonly float height = 1.6f;

		public readonly float outerRadius = 0.5f;
		public readonly float innerRadius = 0.24f;
		public readonly float stepHeight = 0.3f;
		public readonly float bumpHeight = 0.6f;
		public readonly float headBumpHeight = 1.5f;

		public readonly float groundedRayHeight = 0.78f;
		public readonly float groundedRayLength = 1.78f;
		public readonly float airborneRayHeight = 0.78f;
		public readonly float airborneRayLength = 0.78f;

		protected int waterMask;

		void Start() {
			waterMask = LayerMask.GetMask("Water");
		}

		void OnDrawGizmos() {
			Gizmos.color = Color.blue;
			Gizmos.DrawLine(transform.position + Vector3.up * groundedRayHeight,
					transform.position - Vector3.up * (groundedRayLength - groundedRayHeight));

			Gizmos.color = Color.red;
			Vector3 center = transform.position + Vector3.up * stepHeight;
			Gizmos.DrawLine(center - Vector3.right * innerRadius, center + Vector3.right * innerRadius);
			Gizmos.DrawLine(center - Vector3.forward * innerRadius, center + Vector3.forward * innerRadius);

			center = transform.position + Vector3.up * bumpHeight;
			Gizmos.DrawLine(center - Vector3.right * outerRadius, center + Vector3.right * outerRadius);
			Gizmos.DrawLine(center - Vector3.forward * outerRadius, center + Vector3.forward * outerRadius);

			center = transform.position + Vector3.up * headBumpHeight;
			Gizmos.DrawLine(center - Vector3.right * outerRadius, center + Vector3.right * outerRadius);
			Gizmos.DrawLine(center - Vector3.forward * outerRadius, center + Vector3.forward * outerRadius);
		}

		bool IsWall(Vector3 normal) {
			return normal.y < 0.035f && normal.y > -0.035f;
		}

		public bool FloorRay(out RaycastHit hit) {
			Physics.queriesHitBackfaces = false;
			if (C.currentState is StateGrounded)
				return Physics.Raycast(C.transform.position +
						Vector3.up * groundedRayHeight, -Vector3.up,
						out hit, groundedRayLength, ~waterMask);
			else
				return Physics.Raycast(C.transform.position +
						Vector3.up * airborneRayHeight, -Vector3.up,
						out hit, airborneRayLength, ~waterMask);
		}

		public bool CeilingRay(out RaycastHit hit) {
			Physics.queriesHitBackfaces = false;
			// First check if there's a ceiling right above Mario
			bool res = Physics.Raycast(C.transform.position, Vector3.up,
					out hit, height, ~waterMask);
			if (!res) {
				Physics.queriesHitBackfaces = true;
				// Now do an infinite raycast downwards
				res = Physics.Raycast(C.transform.position + 
						Vector3.up * airborneRayHeight, -Vector3.up,
						out hit, ~waterMask);
				// Check if the ray hit a ceiling
				res &= hit.normal.y < -0.035f;
			}
			if (!res)
				hit = default(RaycastHit);

			return res;
		}

		public bool OutOfBoundsRay() {
			Physics.queriesHitBackfaces = false;
			return Physics.Raycast(C.transform.position + 
					Vector3.up * airborneRayHeight, -Vector3.up,
					Mathf.Infinity, ~waterMask);
		}

		bool HRay(Vector3 dir, float height, float radius, out RaycastHit hit) {
			// Hit wall backfaces too
			Physics.queriesHitBackfaces = true;

			Vector3 center = transform.position + Vector3.up * height;
			bool result = Physics.Raycast(center, dir, out hit,
					radius * SQRT2, ~waterMask);
			result &= IsWall(hit.normal);

			return result;
		}

		private void ComputePenetrationAtHeight(float height, float radius, out RaycastHit wallHit) {
			wallHit = default(RaycastHit);
			Vector3 displacement = Vector3.zero;
			Vector3[] directions = new Vector3[] { Vector3.right, -Vector3.right, Vector3.forward, -Vector3.forward };
			RaycastHit hit;
			foreach (Vector3 dir in directions) {
				if (HRay(dir, height, radius, out hit)) {
					float dot = Mathf.Abs(Vector3.Dot(hit.normal, dir));
					float maxDist = radius * 1f / dot;
					// Let only one direction pick 45deg walls
					float error = Mathf.Abs(dir.z) * 0.001f;

					if (hit.distance <= maxDist && dot - error > SQRT2 / 2f) {
						Vector3 newPos = hit.point + hit.normal * radius - 
							dir * hit.distance - hit.normal * dot * hit.distance;

						displacement += newPos - transform.position;
					}
					// Save wall hit info
					wallHit = hit;
				}
			}
			displacement.y = 0;
			transform.position += displacement;
		}

		public void HandleWalls() {
			RaycastHit hit;
			C.wall = default(RaycastHit);
			if (C.currentState is StateGrounded) {
				ComputePenetrationAtHeight(bumpHeight, outerRadius, out hit);
				C.wall = hit;
				ComputePenetrationAtHeight(stepHeight, innerRadius, out hit);
				//ComputePenetrationAtHeight(height - bumpHeight, outerRadius);
			}
			else if (C.currentState is StateAirborne) {
				ComputePenetrationAtHeight(headBumpHeight, outerRadius, out hit);
				C.wall = hit;
				ComputePenetrationAtHeight(stepHeight, outerRadius, out hit);
			}
		}

		public void HandleFloor() {
			RaycastHit floor;
			if (FloorRay(out floor)) {
				Vector3 newPos = transform.position;
				newPos.y = floor.point.y;
				transform.position = newPos;
			}
			C.floor = floor;
		}

		public void HandleCeiling() {
			// The game doesn't push Mario out of a ceiling, the movement
			// gets cancelled instead
			RaycastHit ceiling;
			CeilingRay(out ceiling);
			C.ceiling = ceiling;
		}

		public void HandleWater() {
			Physics.queriesHitBackfaces = true;

			RaycastHit waterHit;
			Physics.Raycast(transform.position, Vector3.up, out waterHit,
					Mathf.Infinity, waterMask);
			C.water = waterHit;
		}

		public void ComputePenetration() {
			HandleWalls();
			HandleFloor();
			HandleCeiling();
		}
	}
}
