﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SoundBank : MonoBehaviour
{
	[System.Serializable]
	public class ClipDictionary : SerializableDictionary<string, AudioClip> { }

	[SerializeField]
	private ClipDictionary _soundDictionary;

	public ClipDictionary sounds {
		get {
			return _soundDictionary;
		}
	}
}
