﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class HandBoneHandle : MonoBehaviour {

	public GameObject targetBone;

	// This values are messed up, but whatever...
	//hiddenPosition = -0.1
	//shownPosition = -0.3
	float midPoint = -0.2f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void LateUpdate () {
	
		float handlePosition = this.transform.localPosition.x;

		float targetScale = 0f;
		if (handlePosition < midPoint) {
			targetScale = 1f;
		}

		targetBone.transform.localScale = new Vector3(targetScale, targetScale, targetScale);
	}
}
