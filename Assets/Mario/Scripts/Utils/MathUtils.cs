using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MathUtils
{
	/// <summary>
	/// Kinda like lerping towards something but with capped speed per frame.
	/// </summary>
	/// <param name="transform">The Transform to rotate.</param>
	/// <param name="position">Point in space to look towards.</param>
	/// <param name="turnSpeed">The max angle in degrees.</param>
	public static void SlowTurnAt(Transform transform, Vector3 position, float turnSpeed)
	{
		Vector3 direction = (position - transform.position).normalized;
		SlowTurn(transform, direction, turnSpeed);
	}

	/// <summary>
	/// Kinda like lerping towards something but with capped speed per frame.
	/// </summary>
	/// <param name="transform">The Transform to rotate.</param>
	/// <param name="direction">In which direction.</param>
	/// <param name="turnSpeed">The max angle in degrees.</param>
	public static void SlowTurn(Transform transform, Vector3 direction, float turnSpeed)
	{
		if (direction == Vector3.zero)
			return;

		Vector3 current = transform.forward;

		float angle = Vector3.SignedAngle(current, direction, Vector3.up);

		transform.Rotate(Vector3.up, Mathf.Clamp(angle, -turnSpeed, turnSpeed));
	}

	/// <summary>
	/// Mantains the direction between the vectors but snaps the second
	/// to the given distance from the first.
	/// </summary>
	/// <param name="vector">The reference vector.</param>
	/// <param name="point">The one to move.</param>
	/// <param name="distance">How far apart.</param>
	/// <returns></returns>
	public static Vector3 FixDistanceToPoint(Vector3 vector, Vector3 point, float distance)
	{
		Vector3 directionToPoint = (point - vector).normalized;
		Vector3 result = point - directionToPoint * distance;

		return result;
	}
	
	/// <summary>
	/// Like <see cref="Physics.Raycast(Ray, float, int)"/> but detects backfaces by
	/// casting another ray in the opposite direction, effectively being twice as
	/// slow!
	/// </summary>
	/// <param name="origin">The origin of the ray.</param>
	/// <param name="vector">The direction AND magnitude. Careful!</param>
	/// <param name="layerMask"></param>
	/// <returns></returns>
	public static bool BothWaysRaycast(Vector3 origin, Vector3 vector, int layerMask = 1)
	{
		float distance = vector.magnitude;

		Ray rayIn = new Ray(origin + vector, -vector);
		Ray rayOut = new Ray(origin, vector);

		bool result = Physics.Raycast(rayIn, distance, layerMask) ||
				Physics.Raycast(rayOut, distance, layerMask);

		Debug.DrawLine(origin, origin + vector, result ? Color.red : Color.green);

		return result;
	}

	public static Matrix4x4 RotationByAxisAngleMatrix(float angle, Vector3 axis)
	{
		float sin = Mathf.Sin(-angle * Mathf.Deg2Rad);
		float cos = Mathf.Cos(-angle * Mathf.Deg2Rad);

		Matrix4x4 result = new Matrix4x4(
			new Vector4(cos + axis.x * axis.x * (1 - cos),				axis.x * axis.y * (1 - cos) - axis.z * sin,		axis.x * axis.z * (1 - cos) + axis.y * sin,		0),
			new Vector4(axis.y * axis.x * (1 - cos) + axis.z * sin,		cos + axis.y * axis.y * (1 - cos),				axis.y * axis.z * (1 - cos) - axis.x * sin,		0),
			new Vector4(axis.z * axis.x * (1 - cos) - axis.y * sin,		axis.z * axis.y * (1 - cos) + axis.x * sin,		cos + axis.z * axis.z * (1 - cos),				0),
			new Vector4(0,												0,												0,												1)
		);

		return result;
	}
}
