﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mario;

public class MarioGraphicsEvents : MonoBehaviour
{
	private Controller C;

    // Start is called before the first frame update
    void Start() {
        C = GetComponentInParent<Controller>();
    }

	void Step() {
		C.PlayStepSound();
	}
}
