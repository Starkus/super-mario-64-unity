using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mario.States;


namespace Mario
{
	/// <summary>
	/// Component of Mario that manages state changes.
	/// It's pretty simple actually...
	/// </summary>
	public class StateMachine : MonoBehaviour
	{

		private Controller C;

		[SerializeField]
		private State _currentState;

		/// <summary>
		/// Current state of the machine. Feel free to replace for
		/// any new instance of any <see cref="State"/> child, it'll
		/// do the rest.
		/// </summary>
		public State currentState
		{
			get
			{
				return _currentState;
			}
		}

		public bool grounded
		{
			get
			{
				return _currentState is Mario.States.StateGrounded;
			}
		}

		public bool airborne
		{
			get
			{
				return _currentState is Mario.States.StateAirborne;
			}
		}


		void Start() {
			C = GetComponent<Controller>();

			_currentState = new StateIdle();
			_currentState.InternalStart(C, null);
		}


		public void NewState<T>() where T : new()
		{
			State previous = _currentState;
			State newState = new T() as State;
			previous.InternalEnd(newState);

			_currentState = newState;
			_currentState.InternalStart(C, previous);
			// If it didn't change
			if (_currentState == newState)
				_currentState.InternalUpdate();
		}
	}
}
