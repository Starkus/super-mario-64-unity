using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Mario.States;


namespace Mario
{
	/// <summary>
	/// Main component of the main character
	/// </summary>
	[RequireComponent(typeof(InputManager))]
	[RequireComponent(typeof(StateMachine))]
	[RequireComponent(typeof(RayCaster))]	
	public class Controller : MonoBehaviour
	{
		public UnityEngine.Camera activeCamera;

		private StateMachine _stateMachine;
		private Animator _animator;
		private InputManager _inputManager;
		private RayCaster _rayCaster;
		private AudioSource _voiceSource;
		[SerializeField]
		private AudioSource _sfxSource;
		private SoundBank _soundBank;
		private MaterialBank _materialBank;

		public RaycastHit wall { get; set; }
		public RaycastHit floor { get; set; }
		public RaycastHit ceiling { get; set; }
		public RaycastHit water { get; set; }

		[SerializeField]
		private float _speed, _vspeed, _xspeed;

		/*
		 * All these properties initialize themselves if needed.
		 */
		public StateMachine stateMachine {
			get {
				if (_stateMachine == null)
					_stateMachine = GetComponent<StateMachine>();
				return _stateMachine;
			}
		}
		public Animator animator {
			get {
				if (_animator == null)
					_animator = GetComponentInChildren<Animator>();
				return _animator;
			}
		}
		public InputManager input {
			get {
				if (_inputManager == null)
					_inputManager = GetComponent<InputManager>();
				return _inputManager;
			}
		}
		public RayCaster rayCaster {
			get {
				if (_rayCaster == null)
					_rayCaster = GetComponent<RayCaster>();
				return _rayCaster;
			}
		}
		public AudioSource voiceSource {
			get {
				if (_voiceSource == null)
					_voiceSource = GetComponent<AudioSource>();
				return _voiceSource;
			}
		}
		public AudioSource sfxSource {
			get {
				if (_sfxSource == null)
					_sfxSource = GetComponentInChildren<AudioSource>();
				return _sfxSource;
			}
		}
		public SoundBank soundBank {
			get {
				if (_soundBank == null)
					_soundBank = GetComponent<SoundBank>();
				return _soundBank;
			}
		}
		public MaterialBank materialBank {
			get {
				if (_materialBank == null)
					_materialBank = GetComponent<MaterialBank>();
				return _materialBank;
			}
		}

		/// <summary>
		/// Shortcut for stateMachine.currentState.
		/// </summary>
		public State currentState
		{
			get
			{
				return stateMachine.currentState;
			}
		}

		/// <summary>
		/// Speed along local Z axis. Can be negative.
		/// </summary>
		public float speed
		{
			get
			{
				return _speed;
			}

			set
			{
				_speed = value;
			}
		}

		/// <summary>
		/// Vertical speed. Along global Y axis.
		/// </summary>
		public float vspeed
		{
			get
			{
				return _vspeed;
			}

			set
			{
				_vspeed = value;
			}
		}

		/// <summary>
		/// Lateral speed. Local X axis. Used commonly for air control.
		/// </summary>
		public float xspeed
		{
			get
			{
				return _xspeed;
			}

			set
			{
				_xspeed = value;
			}
		}

		public Vector3 velocity {
			get {
				return new Vector3(_xspeed, _vspeed, _speed);
			}
			set {
				speed = value.z;
				vspeed = value.y;
				xspeed = value.x;
			}
		}

		public float pitch { get; set; }
		public float yaw { get; set; }
		public float roll { get; set; }

		/// <summary>
		/// Short for stateMachine.grounded
		/// </summary>
		public bool grounded
		{
			get
			{
				return stateMachine.grounded;
			}
		}

		/// <summary>
		/// Short for stateMachine.grounded
		/// </summary>
		public bool airborne
		{
			get
			{
				return stateMachine.airborne;
			}
		}


		private void Start()
		{
			_stateMachine = GetComponent<StateMachine>();
			_animator = GetComponentInChildren<Animator>();

			if (activeCamera == null)
				activeCamera = UnityEngine.Camera.main;
		}

		private void Update()
		{
			_stateMachine.currentState.InternalUpdate();
		}

		private void FixedUpdate()
		{
			_stateMachine.currentState.InternalFixedUpdate();
		}


		public void NewState<T>() where T : new() {
			_stateMachine.NewState<T>();
		}

		public void Move() {
			bool hitCeilingFromAbove = false;
			bool hitCeilingFromBelow = false;
			bool outOfBounds = false;

			RaycastHit newCeiling = default(RaycastHit);

			// There's an initial wall check before moving
			rayCaster.HandleWalls();

			for (int i = 0; i < 4; ++i) {
				// Save old position in case we need to revert changes
				Vector3 oldPos = transform.position;
				RaycastHit oldWall = wall;
				RaycastHit oldFloor = floor;

				// Move a quarter step
				transform.position += currentState.ComputeVelocity() * 0.0025f;

				rayCaster.ComputePenetration();

				if (floor.collider != null && vspeed > 0) {
					vspeed = 0;
				}

				if (!rayCaster.OutOfBoundsRay()) {
					transform.position = new Vector3(
							oldPos.x,
							transform.position.y,
							oldPos.z);
					outOfBounds = true;
					rayCaster.HandleFloor();
					continue;
				}

				if (ceiling.collider != null) {
					if (currentState is StateAirborne) {
						if (vspeed > 0 || hitCeilingFromBelow) {
							hitCeilingFromBelow = true;
							vspeed = 0;
							// Remember the ceiling
							newCeiling = ceiling;
							// Cancel quarter step
							transform.position = oldPos;
						}
						else {
							hitCeilingFromAbove = true;
							transform.position = new Vector3(
									oldPos.x,
									transform.position.y,
									oldPos.z);
						}
					}
					else {
						transform.position = oldPos;
					}
					//wall = oldWall;
					//floor = oldFloor;
				}
			}
			if ((hitCeilingFromAbove || outOfBounds) && speed > 0) {
				speed = 0;
			}
			else if (hitCeilingFromBelow) {
				ceiling = newCeiling;
			}
			
			rayCaster.HandleWater();
		}

		public bool IsFacingSlope() {
			if (floor.collider == null || floor.normal.y == 1f)
				return false;

			Vector3 flatNor = floor.normal;
			flatNor.y = 0;
			float angle = Vector3.Angle(transform.forward, flatNor);

			return angle >= 90f;
		}

		public void PlayVoiceClip(string key) {
			voiceSource.Stop();

			AudioClip clip = soundBank.sounds[key];
			voiceSource.PlayOneShot(clip, 1f);
		}

		public void PlayRandomVoiceClip(string[] keys) {
			int random = Random.Range(0, keys.Length);
			PlayVoiceClip(keys[random]);
		}

		public void PlaySFX(string key) {
			AudioClip clip = soundBank.sounds[key];
			sfxSource.PlayOneShot(clip, 1f);
		}

		private MyPhysicMaterial ToMyPhysicMaterial(PhysicMaterial mat) {
			string matName = mat.name.Split(' ')[0];
			return materialBank.materials[matName];
		}

		public void PlayStepSound() {
			if (floor.collider == null)
				return;

			var mat = floor.collider.material;
			var myMat = ToMyPhysicMaterial(mat);
			AudioClip stepClip = myMat.stepSound;
			sfxSource.PlayOneShot(stepClip, 1f);
		}
		
		public void PlayJumpSound() {
			if (floor.collider == null)
				return;

			var mat = floor.collider.material;
			var myMat = ToMyPhysicMaterial(mat);
			AudioClip jumpClip = myMat.jumpSound;
			sfxSource.PlayOneShot(jumpClip, 1f);
		}

		public void PlayLandSound() {
			if (floor.collider == null)
				return;

			var mat = floor.collider.material;
			var myMat = ToMyPhysicMaterial(mat);
			AudioClip landClip = myMat.landSound;
			sfxSource.PlayOneShot(landClip, 1f);
		}
	}
}
