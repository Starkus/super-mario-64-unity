﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StateLongJump : StateJumpBase {
		public override void PlayAnimation(bool reset = true) {
			string animation = C.speed > 20f ? "LongJump" : "LongJumpSlow";
			C.animator.CrossFade(animation, animationCrossfade, -1, 0);
		}

		public override void PlayVoiceClip() {
			C.PlayVoiceClip("Yahoo");
		}

		public override void Start() {
			jumpForce = 32f;
			gravity = -2f;
			speedUpperSoftcap = 48f;

			C.speed = Mathf.Min(48f, C.speed * 1.5f);

			base.Start();
		}

		public override void FixedUpdate() {
			if (LedgeGrabCheck())
				return;

			if (BonkCheck())
				return;

			base.FixedUpdate();
		}

		public override void OnLand() {
			NewState<StateLandLongJump>();
		}
    }
}
