﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StateBonk : StateBonkSoft {
		public override void PlayVoiceClip() {
			C.PlayVoiceClip("Doh");
		}

		public override void Start() {
			base.Start();

			if (currentState == this)
				C.speed = -16f;
		}

		public override void OnLand() {
			NewState<StateLandBonk>();
		}
    }
}
