﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StateTurnAround : StateRun
	{
		public override void Start() {
			transform.Rotate(0, 180f, 0, Space.Self);

			C.speed = Mathf.Min(8f, C.input.analogTilt);
		}

		public override void Update() {
			if (C.input.GetButtonDown("Jump")) {
				NewState<StateSideJump>();
				return;
			}

			base.Update();
		}

		public override void FixedUpdate() {
			if (fixedTimer > 12) {
				NewState<StateRun>();
				return;
			}

			base.FixedUpdate();
		}
	}
}
