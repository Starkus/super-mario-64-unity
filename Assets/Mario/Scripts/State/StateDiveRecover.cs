﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StateDiveRecover : StateJumpBase
	{
		public override void Start() {
			jumpForce = 30f;

			base.Start();
		}

		public override void FixedUpdate() {
			base.FixedUpdate();

			if (fixedTimer == 6)
				C.PlaySFX("Wind01");
		}

		public override void OnLand() {
			NewState<StateLandDiveRecover>();
		}
	}
}
