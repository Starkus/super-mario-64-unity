﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StateRunningPunch : StatePunch
	{
		public override void Start() {
			if (previousState is StateRunningPunch)
				combo = (previousState as StateRunningPunch).combo + 1;
		}
			
		public override void Update() {
			if (fixedTimer > 5 && C.input.GetButtonDown("Attack") && combo < 2) {
				NewState<StateRunningPunch>();
				return;
			}
		}
		// Update is called once per frame
		public override void FixedUpdate() {
			if (fixedTimer > 14) {
				if (C.input.analogTilt > 0)
					NewState<StateRun>();
				else
					NewState<StateIdle>();
				return;
			}

			PunchWall();

			Friction();
		}
	}
}
