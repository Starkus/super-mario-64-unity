using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Mario.States
{
	public class StateRun : StateRunBase
	{
		public override void Start()
		{
			base.Start();

			if (C.speed == 0 && !(previousState is StateRunBase))
				C.speed = Mathf.Min(8f, C.input.analogTilt);

			if (previousState is StateIdle)
				MathUtils.SlowTurn(transform, C.input.worldInputVector, 1000f);
		}

		public override void Update() {
			PlayAnimation(false);

			if (C.input.GetButtonDown("Jump")) {
				Jump<StateJump>();
				return;
			}

			if (DiveAndJumpKick())
				return;

			if (C.input.GetButtonDown("Attack")) {
				NewState<StateRunningPunch>();
				return;
			}

			if (C.input.GetButton("Duck")) {
				NewState<StateDuck>();
				return;
			}
		}

		public override void FixedUpdate()
		{
			base.FixedUpdate();
			if (currentState != this)
				return;

			if (TurnAroundCheck())
				return;

			if (PushCheck())
				return;

			if (SlideCheck())
				return;

			if (Mathf.Abs(C.speed) < 0.01f)
			{
				NewState<StateIdle>();
				return;
			}

			Turn();

			if (Acceleration())
				return;

			Hills();

			FloorPitch();
		}
	}
}
