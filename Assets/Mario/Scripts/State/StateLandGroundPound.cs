using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Mario.States
{
	public class StateLandGroundPound : StateLandBase
	{
		public override void PlayLandingSFX() {
			C.PlaySFX("GroundPound");
		}

		public override void Update() {
			if (fixedTimer > 6) {
				if (C.input.GetButtonDown("Jump")) {
					NewState<StateJump>();
					return;
				}
			}
		}

		public override void FixedUpdate() {
			if (fixedTimer == 6)
				C.PlayLandSound();

			if (fixedTimer > 6) {
				if (C.input.analogTilt > 0) {
					NewState<StateRun>();
					return;
				}
			}

			if (fixedTimer > 27) {
				NewState<StateIdle>();
				return;
			}
		}
	}
}
