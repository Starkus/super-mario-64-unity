using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Mario.States
{
	public class StateGrabLedge : StateGrounded
	{
		private bool tiltPrevious = true;

		public override void PlayVoiceClip() {
			C.PlayVoiceClip("Whoa");
		}

		public override void Start()
		{
			C.speed = 0;
			C.xspeed = 0;
		}

		public override void Update()
		{
			if (C.input.GetButtonDown("Jump")) {
				NewState<StatePullUpLedge>();
				return;
			}

			if (C.input.GetButtonDown("Duck")) {
				Release();
				return;
			}
		}

		public override void FixedUpdate()
		{
			if (!tiltPrevious && C.input.analogTilt > 0) {
				float angle = Vector3.Angle(transform.forward, C.input.worldInputVector);
				if (angle > 135f) {
					Release();
					return;
				}
				else if (angle < 45f) {
					NewState<StateClimbLedge>();
					return;
				}
			}

			tiltPrevious = C.input.analogTilt > 0;

			if (IsFloorSteep()) {
				Release();
				return;
			}
		}

		protected void Release() {
			transform.position -= transform.forward * C.rayCaster.outerRadius * 2;
			transform.position -= Vector3.up * C.rayCaster.height;
			NewState<StateBonkSoft>();
		}
	}
}
