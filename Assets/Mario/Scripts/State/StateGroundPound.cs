﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StateGroundPound : StateAirborne
	{
		public override void PlayVoiceClip() {
			C.PlaySFX("Wind04");
		}

		public override void Start() {
			base.Start();

			C.speed = 0f;
			C.vspeed = -50f;
		}

		public override void Update() { }

		public override void FixedUpdate() {
			if (fixedTimer == 15)
				C.PlayVoiceClip("Wa");

			if (fixedTimer < 16)
				C.vspeed = -50f;

			if (BonkCheck())
				return;
		}

		public override Vector3 ComputeVelocity() {
			if (fixedTimer < 16) {
				if (C.ceiling.collider != null)
					return Vector3.zero;
				else
					return Vector3.up * Mathf.Max(0, 20f - fixedTimer * 2f);
			}
			else {
				return base.ComputeVelocity();
			}
		}

		public override void OnLand() {
			NewState<StateLandGroundPound>();
		}
	}
}
