﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StateTripleJump : StateJumpBase {
		public override void PlayVoiceClip() {
			C.PlayRandomVoiceClip(new[] { "Yahoo", "Yahoo", "Yahoo", "Waha", "Yippie" });
		}

		public override void Start() {
			jumpForce = 69f;

			base.Start();
		}

		public override void Update() {
			base.Update();

			if (C.input.GetButtonDown("Attack")) {
				NewState<StateDive>();
				return;
			}

			if (GroundPoundCheck())
				return;
		}

		public override void FixedUpdate() {
			if (LedgeGrabCheck())
				return;

			if (BonkCheck())
				return;

			base.FixedUpdate();

			if (fixedTimer == 4)
				C.PlaySFX("Wind03");
		}

		public override void OnLand() {
			NewState<StateLandTripleJump>();
			return;
		}
    }
}
