using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Mario.States
{
	public abstract class StateWater : State
	{
		public sealed override void InternalStart(Controller controller, State previous)
		{
			base.InternalStart(controller, previous);
		}

		public sealed override void InternalUpdate()
		{
			base.InternalUpdate();
		}

		public sealed override void InternalFixedUpdate()
		{
			base.InternalFixedUpdate();

			C.Move();

			if (C.water.collider == null) {
				OnExitedWater();
			}
		}

		public sealed override void InternalEnd(State next)
		{
			base.InternalEnd(next);
		}

		public override void FixedUpdate() {
			base.FixedUpdate();
		}

		public virtual void OnExitedWater() {
			NewState<StateFall>();
		}
	}
}
