using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Mario.States
{
	public class StateLandBase : StateRunBase
	{
		public override void PlayVoiceClip() { }

		public override void Update()
		{
			if (fixedTimer <= 8 && C.input.GetButtonDown("Jump")) {
				Jump<StateDoubleJump>();
				return;
			}
		}

		public override void FixedUpdate()
		{
			float friction = C.floor.collider.material.staticFriction;
			if (friction < 0.1f && IsFloorSteep()) {
				if (C.IsFacingSlope())
					NewState<StateSlideBelly>();
				else
					NewState<StateSlideButt>();
				return;
			}

			if (SlideCheck())
				return;

			base.FixedUpdate();

			if (C.input.inputVector.magnitude > 0) {
				C.speed *= 0.98f;
			}
			else {
				if (Mathf.Abs(C.speed) <= 4f)
					C.speed = 0f;
				else if (C.speed > 0)
					C.speed -= 4f;
				else
					C.speed += 4f;
			}

			Hills();
		}
	}
}
