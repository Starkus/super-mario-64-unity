using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Mario.States
{
	public class StateSlideBellyEnd : StateIdle
	{
		public override void Update()
		{
		}

		public override void FixedUpdate() {
			if (fixedTimer > 37)
				NewState<StateIdle>();
		}
	}
}
