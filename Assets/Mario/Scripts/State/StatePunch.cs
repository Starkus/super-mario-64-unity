﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StatePunch : StateRunBase
	{
		public int combo = 0;
		bool punchedWall = false;

		public override void PlayAnimation(bool reset = true) {
			switch (combo) {
				case 0:
					C.animator.Play("Punch");
					break;
				case 1:
					C.animator.Play("Punch2");
					break;
				case 2:
					C.animator.Play("JumpKick");
					break;
			}
		}

		public override void PlayVoiceClip() {
			switch (combo) {
				case 0:
					C.PlayVoiceClip("Yah");
					break;
				case 1:
					C.PlayVoiceClip("Wah");
					break;
				case 2:
					C.PlayVoiceClip("Hoo");
					break;
			}
		}

		public override void Start() {
			if (previousState is StatePunch)
				combo = (previousState as StatePunch).combo + 1;

			if (combo == 0)
				C.speed = 10f;
		}
			
		public override void Update() {
			base.Update();

			if (fixedTimer > 5 && C.input.GetButtonDown("Attack") && combo < 2) {
				NewState<StatePunch>();
				return;
			}
		}
		// Update is called once per frame
		public override void FixedUpdate() {
			if (fixedTimer > 14) {
				NewState<StateIdle>();
				return;
			}

			if (!punchedWall && C.input.analogTilt > 0) {
				NewState<StateRun>();
				return;
			}

			PunchWall();

			Friction();
		}

		protected void PunchWall() {
			RaycastHit hit;
			bool wallInFront = Physics.Raycast(transform.position +
					Vector3.up * C.rayCaster.bumpHeight, transform.forward,
					out hit, C.rayCaster.outerRadius + 0.1f);

			if (fixedTimer >= 4 && fixedTimer < 10 && wallInFront) {
				Vector3 flatNor = hit.normal;
				flatNor.y = 0;
				float angle = Vector3.Angle(transform.forward, -flatNor);
				if (angle < 45f) {
					C.speed = -48f;
					punchedWall = true;
					C.PlaySFX("PunchWall");
				}
			}
		}

		protected void Friction() {
			float friction = punchedWall ? 8f : 1f;
			
			if (C.speed > friction)
				C.speed -= friction;
			else if (C.speed < -friction)
				C.speed += friction;
			else
				C.speed = 0;
		}
	}
}
