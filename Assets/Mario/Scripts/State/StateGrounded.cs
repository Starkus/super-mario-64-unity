using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Mario.States
{
	public abstract class StateGrounded : State
	{
		public sealed override void InternalStart(Controller controller, State previous)
		{
			base.InternalStart(controller, previous);

			PlayLandingSFX();

			C.xspeed = 0;
			C.vspeed = 0;
		}

		public sealed override void InternalUpdate()
		{
			base.InternalUpdate();
		}

		public sealed override void InternalFixedUpdate()
		{
			C.Move();

			if (C.floor.collider == null) {
				OnFall();
				return;
			}

			if (C.water.collider != null) {
				OnEnteredWater();
				return;
			}

			base.InternalFixedUpdate();
		}

		public sealed override void InternalEnd(State next)
		{
			base.InternalEnd(next);
			
			C.animator.transform.localEulerAngles = Vector3.zero;
		}

		/// <summary>
		/// Called when the physics stopped detecting a floor under the
		/// character.
		/// </summary>
		public virtual void OnFall() {
			NewState<StateFall>();
		}

		public virtual void OnEnteredWater() {
			NewState<StateWaterEnter>();
		}

		protected bool IsFloorSteep() {
			float friction = C.floor.collider.material.staticFriction;
			float y = C.floor.normal.y;
			if (friction > 0.9f || y > 0.9998477f)
				return false;
			else if (friction > 0.5f)
				return y < 0.7880108f;
			else if (friction > 0.2f)
				return y < 0.9396926f;
			else if (friction > 0.1f)
				return y < 0.9848077f;
			else
				return true;
		}
		
		protected bool DiveAndJumpKick() {
			if (C.input.GetButtonDown("Attack")) {
				if ((C.speed < 29f || C.input.analogTilt < 18f)) {
					if (C.input.GetButton("Jump")) {
						NewState<StateJumpKick>();
						return true;
					}
				}
				else {
					NewState<StateGroundDive>();
					return true;
				}
			}
			return false;
		}

		protected void Jump<T>() where T : new() {
			if (!IsFloorSteep() || !C.IsFacingSlope())
				NewState<T>();
			else
				NewState<StateSlopeJump>();
		}

		public virtual void PlayLandingSFX() {
			if (previousState != null && previousState is StateAirborne) {
				C.PlayLandSound();
			}
		}
	}
}
