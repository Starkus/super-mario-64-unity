﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StateSlideKick : StateJumpBase {
		bool bounced = false;

		public override void Start() {
			jumpForce = 14f;
			C.speed = Mathf.Max(32f, C.speed);
			gravity = -2f;

			base.Start();
		}

		public override void Update() { }

		public override void FixedUpdate() {
			base.FixedUpdate();
		}

		public override void OnLand() {
			if (!bounced) {
				C.vspeed *= -0.5f;
				bounced = true;
			}
			else {
				NewState<StateLandSlideKick>();
			}
		}
    }
}
