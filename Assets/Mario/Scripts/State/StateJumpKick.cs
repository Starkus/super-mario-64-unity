﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StateJumpKick : StateJumpBase {
		public override void PlayVoiceClip() {
			C.PlayVoiceClip("Hoo");
		}

		public override void Start() {
			jumpForce = 20f;

			base.Start();
		}

		public override void FixedUpdate() {
			if (fixedTimer >= 2 && fixedTimer < 10 && C.wall.collider != null) {
				Vector3 flatNor = C.wall.normal;
				flatNor.y = 0;
				float angle = Vector3.Angle(transform.forward, -flatNor);
				if (angle < 45f) {
					C.speed = -48f;
					C.PlaySFX("PunchWall");
				}
			}

			base.FixedUpdate();
		}
    }
}
