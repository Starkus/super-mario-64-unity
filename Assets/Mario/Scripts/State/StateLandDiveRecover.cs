using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Mario.States
{
	public class StateLandDiveRecover : StateLand
	{
		public override void Update() {
			if (C.input.GetButtonDown("Jump")) {
				NewState<StateJump>();
				return;
			}
		}

		public override void FixedUpdate()
		{
			base.FixedUpdate();

			if (C.input.analogTilt > 0 && currentState == this) {
				NewState<StateRun>();
				return;
			}
		}
	}
}
