﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StateDuck : StateSlideBase {
		public override void Update() {
			if (C.input.GetButtonDown("Jump")) {
				if (C.speed >= 10f && fixedTimer < 6)
					Jump<StateLongJump>();
				else if (C.speed == 0)
					Jump<StateBackFlip>();
				else
					Jump<StateJump>();
				return;
			}
			else if (C.input.GetButtonDown("Attack")) {
				if (C.speed >= 10f && fixedTimer < 6) {
					NewState<StateSlideKick>();
					return;
				}
			}
		}

		public override void FixedUpdate() {
			if (IsFloorSteep()) {
				NewState<StateSlideButt>();
				return;
			}

			if (C.speed == 0 && !C.input.GetButton("Duck")) {
				NewState<StateIdle>();
				return;
			}

			base.FixedUpdate();
		}
    }
}
