﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StateSideJump : StateJumpBase {
		public override void Start() {
			jumpForce = 62f;
			transform.LookAt(transform.position + C.input.worldInputVector);
			C.speed = 8f;

			base.Start();
		}

		public override void Update() {
			if (C.input.GetButtonDown("Attack")) {
				NewState<StateDive>();
				return;
			}

			if (GroundPoundCheck())
				return;
		}

		public override void FixedUpdate() {
			if (LedgeGrabCheck())
				return;

			if (BonkCheck())
				return;

			base.FixedUpdate();

			if (fixedTimer == 8)
				C.PlaySFX("Wind02");
		}
    }
}
