﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StateWallJump : StateJumpBase {
		public override void Start() {
			jumpForce = 62f;
			transform.Rotate(0, 180f, 0);
			if (C.speed < 24f)
				C.speed = 24f;

			base.Start();
		}

		public override void Update() {
			base.Update();

			if (C.input.GetButtonDown("Attack")) {
				NewState<StateDive>();
				return;
			}

			if (GroundPoundCheck())
				return;
		}

		public override void FixedUpdate() {
			if (LedgeGrabCheck())
				return;

			if (BonkCheck())
				return;

			base.FixedUpdate();
		}
    }
}
