﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StateDoubleJump : StateJump
	{
		public override void PlayVoiceClip() {
			C.PlayVoiceClip("Woohoo");
		}

		// Start is called before the first frame update
		public override void Start() {
			jumpForce = 52f;

			base.Start();
		}

		public override void OnLand() {
			NewState<StateLandDoubleJump>();
		}
	}
}
