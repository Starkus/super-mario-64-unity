using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Mario.States
{
	public class StateSlideButt : StateSlideBase
	{
		public override void Update() {
			if (C.input.GetButtonDown("Jump")) {
				NewState<StateJump>();
				return;
			}
		}

		public override void FixedUpdate() {
			base.FixedUpdate();

			if (C.speed == 0)
				NewState<StateSlideButtEnd>();
		}
	}
}
