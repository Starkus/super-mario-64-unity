﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StateWaterEnter : StateWaterIdle {
		public override void Start() {
			transform.position = C.water.point - Vector3.up;
			C.speed /= 4f;
			C.vspeed /= 2f;
		}

		public override void FixedUpdate() {
			if (fixedTimer > 18) {
				NewState<StateWaterIdle>();
				return;
			}

			base.FixedUpdate();
		}
    }
}
