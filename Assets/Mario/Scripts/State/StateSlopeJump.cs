﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StateSlopeJump : StateJump
	{
		public override void PlayAnimation(bool reset = true) {
			C.animator.CrossFade("Jump", animationCrossfade);
		}

		public override void Update() {
			if (C.input.GetButtonDown("Attack")) {
				NewState<StateDive>();
				return;
			}
		}


		public override void FixedUpdate() {
			JumpCancel();
		}

		protected override bool LedgeGrabCheck() { return false; }
	}
}
