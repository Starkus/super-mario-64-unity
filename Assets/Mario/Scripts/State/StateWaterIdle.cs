﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StateWaterIdle : StateWater {
		public override void Start() { }

		public override void Update() {
			if (C.input.GetButtonDown("Jump")) {
				NewState<StateWaterSwim>();
				return;
			}
		}

		public override void FixedUpdate() {
			if (C.speed > 1f)
				C.speed -= 1f;
			else if (C.speed < -1f)
				C.speed += 1f;
			else
				C.speed = 0;

			if (C.vspeed > 2f)
				C.vspeed -= 2f;
			else if (C.vspeed < -2f)
				C.vspeed += 2f;
			else
				C.vspeed = 0;
		}
    }
}
