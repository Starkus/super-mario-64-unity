using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Mario.States
{
	public abstract class StateJumpBase : StateAirborne
	{
		protected float jumpForce = -1f;

		public override void PlayVoiceClip() {
			C.PlayRandomVoiceClip(new[] { "Yah", "Wah", "Woo" });
		}

		public override void Start()
		{
			base.Start();

			C.vspeed = jumpForce;
		}

		public override void OnLand()
		{
			base.OnLand();

			NewState<StateLand>();
		}
	}
}
