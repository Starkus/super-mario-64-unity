using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Mario.States {
	public class StateLandLongJump : StateLandBase {
		public override void Update() {
			if (fixedTimer <= 6 && C.input.GetButton("Duck") && C.input.GetButtonDown("Jump")) {
				Jump<StateLongJump>();
				return;
			}
		}

		public override void FixedUpdate() {
			if (fixedTimer > 5 && C.input.analogTilt > 0) {
				NewState<StateRun>();
				return;
			}

			if (fixedTimer > 16) {
				NewState<StateDuck>();
				return;
			}

			base.FixedUpdate();
		}
	}
}
