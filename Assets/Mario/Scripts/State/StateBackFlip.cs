﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StateBackFlip : StateJumpBase {
		public override void Start() {
			jumpForce = 62f;
			C.speed = -16f;

			base.Start();
		}

		public override void Update() {
			base.Update();

			if (GroundPoundCheck())
				return;
		}

		public override void FixedUpdate() {
			if (LedgeGrabCheck())
				return;

			base.FixedUpdate();

			if (fixedTimer == 4 || fixedTimer == 18)
				C.PlaySFX("Wind01");
		}

		public override void OnLand() {
			NewState<StateLandTripleJump>();
			return;
		}
    }
}
