using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States
{
	/// <summary>
	/// Base clase for <see cref="StateMachine"/> states.
	/// </summary>
	public abstract class State
	{
		/// <summary>
		/// A reference to the main character controller.
		/// </summary>
		protected Controller C;

		/// <summary>
		/// Shortcut for C.transform.
		/// </summary>
		public Transform transform { get { return C.transform; } }

		private State _previousState;
		private State _nextState;

		/// <summary>
		/// The crossfade duration for the auto-played animation.
		/// </summary>
		protected float animationCrossfade = 0.03f;

		/// <summary>
		/// The state this one replaced.
		/// </summary>
		public State previousState
		{
			get
			{
				return _previousState;
			}

			private set
			{
				_previousState = value;
			}
		}

		/// <summary>
		/// Short for C.stateMachine.currentState.
		/// </summary>
		protected State currentState
		{
			get
			{
				return C.stateMachine.currentState;
			}
		}

		/// <summary>
		/// The state to replace this one. Only useful on <see cref="End"/>
		/// body.
		/// </summary>
		public State nextState
		{
			get
			{
				return _nextState;
			}

			private set
			{
				_nextState = value;
			}
		}

		protected void NewState<T>() where T : new() {
			C.NewState<T>();
		}

		/// <summary>
		/// The time in frames since this state was created. To use in
		/// <see cref="Update"/>
		/// </summary>
		public ulong timer { get; protected set; }
		/// <summary>
		/// The time in frames since this state was created. To use in
		/// <see cref="FixedUpdate"/>
		/// </summary>
		public ulong fixedTimer { get; protected set; }

		//public double lifetime { get { return timer; } }


		/// <summary>
		/// <para>Called when the state replaces the previous one. 
		/// The previous state should be accessible with the <see cref="previousState"/>
		/// property.</para>
		/// <para>This is called right after the last state's <see cref="End"/></para>
		/// This function is empty by default.
		/// </summary>
		public virtual void Start() { }

		/// <summary>
		/// Called every rendering frame by <see cref="StateMachine"/>.
		/// This function is empty by default.
		/// </summary>
		public virtual void Update() { }

		/// <summary>
		/// Called every physics frame by <see cref="StateMachine"/>
		/// This function is empty by default.
		/// </summary>
		public virtual void FixedUpdate() { }

		/// <summary>
		/// <para>Called just before swapping states. Mainly used for
		/// cleanup. The replacing state should be accessible with
		/// the <see cref="nextState"/> property.</para>
		/// This function is empty by default.
		/// </summary>
		public virtual void End() { }


		/// <summary>
		/// Called by <see cref="StateMachine"/> before setting as
		/// current state. Should only be overriden by low level states
		/// such as <see cref="StateGrounded"/>
		/// </summary>
		/// <param name="controller">A reference to the main character controller</param>
		/// <param name="previous">The state to be replaced. See: <seealso cref="previousState"/></param>
		public virtual void InternalStart(Controller controller, State previous)
		{
			C = controller;
			previousState = previous;

			Start();

			if (currentState == this) {
				PlayAnimation();
				PlayVoiceClip();
			}
		}

		/// <summary>
		/// <para>The actual function <see cref="StateMachine"/> calls
		/// every render update. Should only be overriden by low level
		/// states such as <see cref="StateGrounded"/></para>
		/// Don't override without calling the base one.
		/// </summary>
		public virtual void InternalUpdate()
		{
			Update();

			timer += 1;
		}

		/// <summary>
		/// <para>The actual function <see cref="StateMachine"/> calls
		/// every render update. Should only be overriden by low level
		/// states such as <see cref="StateGrounded"/></para>
		/// Don't override without calling the base one.
		/// </summary>
		public virtual void InternalFixedUpdate()
		{
			FixedUpdate();

			fixedTimer += 1;
		}

		/// <summary>
		/// <para>The actual function <see cref="StateMachine"/> calls
		/// before switching states.</para>
		/// Don't override without calling the base one.
		/// </summary>
		/// <param name="next">The state to replace this one.</param>
		public virtual void InternalEnd(State next)
		{
			nextState = next;

			End();
		}

		/// <summary>
		/// <para>By default, tries to crossfade into an animator state with
		/// the same name as the class (without "State"). The crossfade
		/// duration can be changed in the <see cref="animationCrossfade"/>
		/// field.</para>
		/// </summary>
		public virtual void PlayAnimation(bool reset = true)
		{
			string[] words = ToString().Split('.');
			string cleanName = words[words.Length - 1].Substring(5);

			int hash = Animator.StringToHash(cleanName);

			if (C.animator != null && C.animator.HasState(0, hash))
			{
				if (reset)
					C.animator.CrossFadeInFixedTime(hash, animationCrossfade);
				else
					C.animator.CrossFade(hash, animationCrossfade);
			}
		}

		public virtual void PlayVoiceClip() { }

		public virtual Vector3 ComputeVelocity() {
			Vector3 velocity = transform.forward * C.speed;
			velocity += Vector3.up * C.vspeed;
			velocity += transform.right * C.xspeed;
			return velocity;
		}
	}
}
