﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StateDive : StateJumpBase {
		public override void PlayVoiceClip() {
			C.PlayVoiceClip("Dive");
		}

		public override void Start() {
			C.speed = Mathf.Min(48f, C.speed + 15);
		}

		public override void FixedUpdate() {
			if (BonkCheck(true))
				return;

			base.FixedUpdate();

			if (C.vspeed <= 0) {
				C.pitch += 1.40625f;
				if (C.pitch > 30f) {
					C.pitch = 30f;
				}
				C.animator.transform.localEulerAngles = new Vector3(C.pitch, 0, 0);
			}
		}

		public override void End() {
			C.pitch = 0;
			C.animator.transform.localEulerAngles = Vector3.zero;
		}

		public override void OnLand() {
			NewState<StateSlideBelly>();
		}
    }
}
