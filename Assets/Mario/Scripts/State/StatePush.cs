﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StatePush : StateRunBase
	{
		public override void Update() {
			base.Update();

			if (C.input.GetButtonDown("Jump")) {
				Jump<StateJump>();
				return;
			}

			if (C.input.GetButtonDown("Attack")) {
				NewState<StateRunningPunch>();
				return;
			}
		}
		// Update is called once per frame
		public override void FixedUpdate() {
			if (C.input.analogTilt == 0) {
				NewState<StateIdle>();
				return;
			}

			if (C.wall.collider == null && C.ceiling.collider == null) {
				NewState<StateRun>();
				return;
			}
			else {
				Vector3 flatNor = C.wall.normal;
				flatNor.y = 0;
				float angle = Vector3.Angle(transform.forward, flatNor);
				if (angle < 135f && C.ceiling.collider == null) {
					NewState<StateRun>();
					return;
				}
			}

			base.FixedUpdate();
			
			Turn();

			C.speed = 6f;
		}
	}
}
