﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StateLandSlideKick : StateSlideBase {
		public override void PlayAnimation(bool reset = true) {
			C.animator.PlayInFixedTime("SlideKick", -1, 1f);
		}

		public override void Update() {
			if (C.input.GetButtonDown("Jump")) {
				NewState<StateDiveRecover>();
				return;
			}
		}

		public override void FixedUpdate() {
			base.FixedUpdate();
		}
    }
}
