using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Mario.States
{
	public class StateLandTripleJump : StateLand
	{
		public override void PlayVoiceClip() { }

		public override void Update() {
			if (C.input.GetButtonDown("Jump")) {
				NewState<StateJump>();
				return;
			}
		}

		public override void FixedUpdate() {
			if (fixedTimer == 3)
				C.PlayVoiceClip("Haha");

			base.FixedUpdate();
		}
	}
}
