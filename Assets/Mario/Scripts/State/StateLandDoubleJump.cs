using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Mario.States
{
	public class StateLandDoubleJump : StateLand
	{
		public override void Update()
		{
			if (fixedTimer <= 8 && C.input.GetButtonDown("Jump")) {
				if (C.speed > 20f)
					NewState<StateTripleJump>();
				else
					NewState<StateJump>();
				return;
			}
		}
	}
}
