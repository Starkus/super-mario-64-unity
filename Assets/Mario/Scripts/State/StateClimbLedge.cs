using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Mario.States
{
	public class StateClimbLedge : StateGrounded
	{
		public override void PlayVoiceClip() {
			C.PlayVoiceClip("PullUp");
		}

		public override void Start()
		{
			C.speed = 0;
			C.xspeed = 0;
		}

		public override void FixedUpdate()
		{
			if (fixedTimer > 27) {
				if (C.input.analogTilt > 0)
					NewState<StateRun>();
				else
					NewState<StateIdle>();
			}
		}
	}
}
