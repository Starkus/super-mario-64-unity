﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StateWaterSwim : StateWater {
		public override void Start() {
		}

		public override void Update() {
			if ((fixedTimer == 8 || fixedTimer >= 20) &&
					C.input.GetButtonDown("Jump")) {
				NewState<StateWaterSwim>();
			}
		}

		public override void FixedUpdate() {
			if (fixedTimer <= 13) {
				if (fixedTimer < 6)
					C.speed += 0.5f;
				else if (fixedTimer >= 9)
					C.speed += 1.5f;

				if (C.speed > 28f)
					C.speed = 28f;

				if (fixedTimer < 2 && fixedTimer >= 6) { }

			}
			else {
				C.speed -= 0.25f;
			}

			if (fixedTimer > 28) {
				NewState<StateWaterIdle>();
				return;
			}
		}
    }
}
