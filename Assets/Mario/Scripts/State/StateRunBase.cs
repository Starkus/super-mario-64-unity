using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Mario.States
{
	public class StateRunBase : StateGrounded
	{
		protected float turnSpeed = 11.25f;

		public override void FixedUpdate()
		{
			float friction = C.floor.collider.material.staticFriction;
			if (friction < 0.1f && IsFloorSteep()) {
				if (C.IsFacingSlope())
					NewState<StateSlideBelly>();
				else
					NewState<StateSlideButt>();
				return;
			}

			base.FixedUpdate();

			C.vspeed = C.xspeed = 0;
		}

		public override Vector3 ComputeVelocity() {
			return base.ComputeVelocity() * C.floor.normal.y;
		}

		protected void Turn(float mult = 1f)
		{
			MathUtils.SlowTurn(transform, C.input.worldInputVector, turnSpeed * mult);
		}

		protected virtual bool TurnAroundCheck(float minSpeed = 16f)
		{
			if (C.speed > minSpeed &&
						Vector3.Angle(C.transform.forward, C.input.worldInputVector) > 100f)
			{
				NewState<StateAboutToTurnAround>();
				return true;
			}

			return false;
		}

		protected void FloorPitch() {
			float pitch = -Vector3.Angle(C.floor.normal, transform.forward) + 90f;
			pitch *= 0.456f;
			C.animator.transform.localEulerAngles = new Vector3(pitch, 0, 0);
		}

		protected virtual bool PushCheck() {
			bool pushingWall = false;
			if (C.wall.collider != null) {
				Vector3 flatNor = C.wall.normal;
				flatNor.y = 0;
				float angle = Vector3.Angle(transform.forward, -flatNor);
				pushingWall = angle < 45f;
			}
			if (C.ceiling.collider != null || pushingWall) {
				if (C.speed > 0) {
					NewState<StatePush>();
					return true;
				}
			}
			return false;
		}

		protected bool Acceleration()
		{
			float inputSpeed = Mathf.Min(32f, C.input.analogTilt);

			// Hard braking
			if (C.speed > 16f && inputSpeed == 0) {
				NewState<StateHardStopping>();
				return true;
			}

			else if (C.speed > inputSpeed) {
				if (C.speed > 2f)
					C.speed -= 2f;
				else if (C.speed < -2f)
					C.speed += 2f;
				else
					C.speed = 0;
			}
				
			else {
				float accel = 1.1f - C.speed / 43f;
				C.speed += Mathf.Min(1.1f, accel);
			}

			return false;
		}

		protected float GetSlopeInfluence() {
			float friction = C.floor.collider.material.staticFriction;
			if (friction > 0.9f)
				return 0;
			else if (friction > 0.5f)
				return 1.7f;
			else if (friction > 0.2f)
				return 2.7f;
			else
				return 5.3f;
		}

		protected void Hills() {
			Vector3 flatNor = C.floor.normal;
			flatNor.y = 0;

			float delta = flatNor.magnitude * GetSlopeInfluence();

			float angle = Vector3.Angle(flatNor, transform.forward);
			if (angle < 90f)
				C.speed += delta;
			else
				C.speed -= delta;
		}

		protected virtual bool SlideCheck() {
			if (!IsFloorSteep())
				return false;

			Vector3 flatNor = C.floor.normal;
			flatNor.y = 0;
			float angle = Vector3.Angle(flatNor, transform.forward);

			if (angle < 90f) {
				NewState<StateSlideButt>();
				return true;
			}
			else if (C.speed < -1f) {
				NewState<StateSlideBelly>();
				return true;
			}
			return false;
		}
	}
}
