﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StateSlideBase : StateGrounded {

		static readonly float PI = Mathf.PI;
		static readonly float PI2 = PI * 2f;
		static readonly float HALFPI = PI / 2f;

		Vector3 slidingVel;

		// Start is called before the first frame update
		public override void Start() {
			slidingVel = transform.forward * C.speed;
		}

		public override Vector3 ComputeVelocity() {
			return slidingVel * C.floor.normal.y;
		}

		protected void AlignGraphicsToGround() {
			Vector3 up = C.floor.normal;
			Vector3 fw = Vector3.Cross(transform.right, up);
			C.animator.transform.LookAt(transform.position + fw, up);
		}

		protected float GetSpeedFactor() {
			float friction = C.floor.collider.material.staticFriction;
			if (friction > 0.9f)
				return 5f;
			else if (friction > 0.5f)
				return 7f;
			else if (friction > 0.2f)
				return 8f;
			else
				return 10f;
		}

		protected float GetInputInfluence() {
			float friction = C.floor.collider.material.staticFriction;
			if (friction > 0.9f)
				return 0.92f;
			else if (friction > 0.5f)
				return 0.92f;
			else if (friction > 0.2f)
				return 0.96f;
			else
				return 0.98f;
		}

		public override void FixedUpdate() {
			if (Bonk())
				return;

			Vector3 inputDirection = C.input.worldInputVector.normalized;
			float analogTilt = C.input.analogTilt;

			float intendedYaw = Mathf.Atan2(inputDirection.x, inputDirection.z);
			float slidingAngle = Mathf.Atan2(slidingVel.x, slidingVel.z);

			float oldSlidingSpeed = slidingVel.magnitude;

			float angleDelta = intendedYaw - slidingAngle;
			float sinDelta = Mathf.Sin(angleDelta);
			float cosDelta = Mathf.Cos(angleDelta);

			if (cosDelta < 0 && C.speed > 0) {
				cosDelta *= (0.5f * C.speed) / 100f + 0.5f;
			}

			float inf = GetInputInfluence();
			float inputSpeedInf = analogTilt / 32f * cosDelta * 0.02f + inf;

			slidingVel.x += slidingVel.z * analogTilt / 32f * sinDelta * 0.05f;
			slidingVel.z -= slidingVel.x * analogTilt / 32f * sinDelta * 0.05f;

			float slidingSpeed = slidingVel.magnitude;

			if (oldSlidingSpeed > 0 && slidingSpeed > 0) {
				slidingVel *= oldSlidingSpeed / slidingSpeed;
			}

			float facingYaw = Mathf.Atan2(transform.forward.x, transform.forward.z);
			// Sliding angle changed
			slidingAngle = Mathf.Atan2(slidingVel.x, slidingVel.z);
			float turnDelta = facingYaw - slidingAngle;

			Vector3 floorNor = C.floor.normal;
			if (floorNor.y < 1.0) {
				float floorAngle = Mathf.Atan2(floorNor.x, floorNor.z);
				float floorXYMag = (new Vector2(floorNor.x, floorNor.z)).magnitude;

				float sinGround = Mathf.Sin(floorAngle);
				float cosGround = Mathf.Cos(floorAngle);

				float f = GetSpeedFactor();

				slidingVel.x += f * floorXYMag * sinGround;
				slidingVel.z += f * floorXYMag * cosGround;

				slidingVel *= inputSpeedInf;

				// Update slidingAngle again
				slidingAngle = Mathf.Atan2(slidingVel.x, slidingVel.z);
				turnDelta = intendedYaw - slidingAngle;

				if (turnDelta > PI)
					turnDelta -= PI2;
				else if (turnDelta < -PI)
					turnDelta += PI2;
			}
			else {
				slidingVel *= inputSpeedInf;

				slidingAngle = Mathf.Atan2(slidingVel.x, slidingVel.z);
				turnDelta = intendedYaw - slidingAngle;
			}

			// 'Reflect' input direction if pointing backwards
			if (C.input.normalInputVector.z < 0)
				turnDelta = PI - turnDelta;

			// turnDelta shouldn't be below -2PI
			float fac = 2.8125f;

			if (turnDelta < -HALFPI) {
				turnDelta -= fac;
				if (turnDelta < -PI)
					turnDelta = -PI;
			}
			else if (turnDelta < 0) {
				turnDelta += fac;
				if (turnDelta > 0)
					turnDelta = 0;
			}
			else if (turnDelta < HALFPI) {
				turnDelta -= fac;
				if (turnDelta < 0)
					turnDelta = 0;
			}
			else {
				turnDelta += fac;
				if (turnDelta > PI)
					turnDelta = PI;
			}

			intendedYaw = slidingAngle + turnDelta;
			Vector3 facingVector = new Vector3(Mathf.Sin(intendedYaw), 0, Mathf.Cos(intendedYaw));

			if (slidingVel.magnitude > 0) {
				MathUtils.SlowTurn(transform, facingVector, 2.8125f);
			}

			C.speed = slidingVel.magnitude;
			if (C.speed > 100f) {
				slidingVel *= 100f / C.speed;
				C.speed = 100f;
			}

			else if (C.speed < 4f && C.speed > -8f && !IsFloorSteep()) {
				C.speed = 0f;
				slidingVel = Vector3.zero;
			}

			if (Vector3.Angle(transform.forward, slidingVel) > 90f)
				C.speed *= -1f;

			AlignGraphicsToGround();
		}

		public override void End() {
			C.animator.transform.LookAt(transform.position + transform.forward);
		}

		protected bool Bonk() {
			if (C.wall.collider != null) {
				Vector3 flatNor = C.wall.normal;
				flatNor.y = 0;
				float angle = Vector3.Angle(transform.forward, -flatNor);

				if (angle < 45f || angle > 135f) {
					if (IsFloorSteep()) {
						slidingVel = Vector3.Reflect(slidingVel, flatNor) * 0.9f;
						C.speed *= -0.9f;
					}
					else {
						NewState<StateLandBonk>();
						C.speed = -32f;
						return true;
					}
				}
			}
			return false;
		}
	}
}
