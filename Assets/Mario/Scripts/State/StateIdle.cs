using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Mario.States
{
	public class StateIdle : StateGrounded
	{
		public StateIdle()
		{
			animationCrossfade = 0.2f;
		}

		public override void Start()
		{
			base.Start();

			C.velocity = Vector3.zero;
		}

		public override void Update()
		{
			base.Update();

			if (C.input.analogTilt > 0.0f) {
				NewState<StateRun>();
				return;
			}

			if (C.input.GetButtonDown("Jump")) {
				NewState<StateJump>();
				return;
			}

			if (C.input.GetButtonDown("Attack")) {
				NewState<StatePunch>();
				return;
			}
			
			if (C.input.GetButton("Duck")) {
				NewState<StateDuck>();
				return;
			}
		}

		public override void FixedUpdate() {
			base.FixedUpdate();

			if (IsFloorSteep()) {
				if (C.IsFacingSlope())
					NewState<StateSlideBelly>();
				else
					NewState<StateSlideButt>();
				return;
			}
		}

		public override void OnFall()
		{
			base.OnFall();

			NewState<StateFall>();
		}
	}
}
