﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StateAboutToTurnAround : StateRunBase
	{
		public override void PlayAnimation(bool reset) {
			C.animator.CrossFadeInFixedTime("HardStopping", animationCrossfade, -1, 0f);
		}

		// Start is called before the first frame update
		public override void Start()
		{
			base.Start();
		}

		public override void Update() {
			if (C.input.GetButtonDown("Jump")) {
				NewState<StateSideJump>();
				return;
			}

			base.Update();

			C.animator.CrossFadeInFixedTime("HardStopping", animationCrossfade, -1, 0f);
		}

		// Update is called once per frame
		public override void FixedUpdate()
		{
			base.FixedUpdate();

			if (Vector3.Angle(C.transform.forward, C.input.worldInputVector) < 100f) {
				NewState<StateRun>();
				return;
			}

			C.speed -= 4f;

			if (C.speed <= 0f) {
				C.speed = 0f;
				NewState<StateTurnAround>();
				return;
			}
		}
	}
}
