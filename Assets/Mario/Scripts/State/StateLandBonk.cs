using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Mario.States
{
	public class StateLandBonk : StateLandBase
	{
		public override void PlayVoiceClip() {
			C.PlayVoiceClip("Oof");
		}

		public override void Update() { }

		public override void FixedUpdate() {
			C.speed *= 0.9f;

			if (fixedTimer > 30) {
				NewState<StateIdle>();
				return;
			}
		}
	}
}
