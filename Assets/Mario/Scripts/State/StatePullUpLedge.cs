using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Mario.States
{
	public class StatePullUpLedge : StateGrounded
	{
		public override void PlayVoiceClip() {
			C.PlayVoiceClip("Ungh");
		}

		public override void Start()
		{
			C.speed = 0;
			C.xspeed = 0;
		}

		public override void FixedUpdate()
		{
			if (fixedTimer > 15) {
				NewState<StateIdle>();
			}
		}
	}
}
