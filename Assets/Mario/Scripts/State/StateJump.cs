﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StateJump : StateJumpBase
	{
		bool releasedButton = false;

		public override void Start() {
			if (jumpForce < 0f)
				jumpForce = 42f;

			jumpForce += C.speed * 0.25f;

			C.speed /= 1.2f;

			base.Start();
		}

		public override void Update() {
			if (C.input.GetButtonDown("Attack")) {
				if (C.speed < 28f)
					NewState<StateJumpKick>();
				else
					NewState<StateDive>();
				return;
			}

			if (GroundPoundCheck())
				return;
		}

		public override void FixedUpdate() {
			base.FixedUpdate();

			if (LedgeGrabCheck())
				return;

			if (BonkCheck())
				return;

			JumpCancel();
		}

		protected void JumpCancel() {
			if (!releasedButton && fixedTimer <= 4 &&
					fixedTimer > 0 && !C.input.GetButton("Jump")) {
				C.vspeed = (C.vspeed + 4f) / 4f;
				releasedButton = true;
			}
		}
	}
}
