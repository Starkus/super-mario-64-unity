using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Mario.States
{
	public class StateFall : StateJumpBase {
		public override void PlayVoiceClip() { }

		public override void Update() {
			base.Update();

			if (C.input.GetButtonDown("Attack")) {
				NewState<StateDive>();
				return;
			}

			if (GroundPoundCheck())
				return;
		}

		public override void FixedUpdate() {
			if (LedgeGrabCheck())
				return;

			base.FixedUpdate();
		}
	}
}
