﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StateStopStopping : State
	{
		// Update is called once per frame
		public override void FixedUpdate()
		{
			if (C.input.analogTilt > 0) {
				NewState<StateRun>();
				return;
			}

			if (timer >= 0.59f) {
				NewState<StateIdle>();
				return;
			}
		}
	}
}
