using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StateLand : StateLandBase {
		public override void Update() {
			if (C.input.GetButtonDown("Jump")) {
				if (fixedTimer <= 8)
					Jump<StateDoubleJump>();
				else
					Jump<StateJump>();
				return;
			}

			if (fixedTimer <= 3 && DiveAndJumpKick())
				return;
		}

		public override void FixedUpdate() {
			if (fixedTimer > 16) {
				NewState<StateIdle>();
				return;
			}
			else if (fixedTimer > 2) {
			   	if (C.input.inputVector.magnitude > 0) {
					NewState<StateRun>();
					return;
				}
				C.speed = 0f;
			}

			if (TurnAroundCheck())
				return;

			base.FixedUpdate();
		}
	}
}
