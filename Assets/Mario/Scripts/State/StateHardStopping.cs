﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StateHardStopping : StateRunBase
	{
		public override void PlayAnimation(bool reset = true) {
			C.animator.CrossFadeInFixedTime("HardStopping", animationCrossfade, -1, 0f);
		}

		public override void Update() {
			base.Update();

			if (C.input.GetButtonDown("Attack")) {
				NewState<StateRunningPunch>();
				return;
			}

			C.animator.CrossFadeInFixedTime("HardStopping", animationCrossfade, -1, 0f);
		}

		// Update is called once per frame
		public override void FixedUpdate()
		{
			if (C.input.analogTilt > 0) {
				NewState<StateRun>();
				return;
			}

			if (TurnAroundCheck())
				return;

			if (C.speed > 4f) {
				C.speed -= 4f;
			}
			else if (C.speed < -4f) {
				C.speed += 4f;
			}
			else {
				C.speed = 0;
				NewState<StateStopStopping>();
				return;
			}
		}
	}
}
