﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StateGroundDive : StateDive {
		public override void PlayVoiceClip() {
			C.PlayRandomVoiceClip(new[] { "Yah", "Wah", "Woo" });
		}

		public override void Start() {
			C.vspeed = 20f;

			base.Start();
		}
    }
}
