using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Mario.States
{
	public abstract class StateAirborne : State
	{
		protected float speedUpperSoftcap = 32.0f;
		protected float speedLowerSoftcap = -16.0f;

		protected float gravity = -4.0f;

		protected bool wallLastFrame = false;

		public sealed override void InternalStart(Controller controller, State previous)
		{
			base.InternalStart(controller, previous);

			PlayJumpingSFX();
		}

		public sealed override void InternalUpdate()
		{
			base.InternalUpdate();
		}

		public sealed override void InternalFixedUpdate()
		{
			C.vspeed += gravity;
			if (C.vspeed < -75f)
				C.vspeed = -75f;

			base.InternalFixedUpdate();
			if (C.currentState != this)
				return;

			C.Move();

			if (C.floor.collider != null) {
				OnLand();
				return;
			}

			if (C.water.collider != null) {
				OnEnteredWater();
				return;
			}
		}

		public sealed override void InternalEnd(State next)
		{
			base.InternalEnd(next);
		}

		public override void FixedUpdate() {
			base.FixedUpdate();

			Input();
		}

		protected void Input() {
			if (C.speed > speedUpperSoftcap) {
				C.speed -= 1.0f;
			}
			else if (C.speed < speedLowerSoftcap) {
				C.speed += 2.0f;
			}

			if (C.speed > 0.35f) {
				C.speed -= 0.35f;
			}
			else if (C.speed < -0.35f) {
				C.speed += 0.35f;
			}
			else {
				C.speed = 0.0f;
			}

			// Input
			float inputAccel = Mathf.Min(1.5f, Mathf.Max(-1.5f,
						C.input.normalInputVector.z / 32f * 1.5f));
			C.speed += inputAccel;

			float latMovement = Mathf.Min(10.0f, Mathf.Max(-10.0f,
						(C.input.normalInputVector.x / 32f) * 10f));
			C.xspeed = latMovement;
		}

		protected virtual bool BonkCheck(bool alwaysHard = false) {
			bool wallBonk = false;
			if (C.wall.collider != null) {
				Vector3 flatNor = C.wall.normal;
				flatNor.y = 0;
				float angle = Vector3.Angle(transform.forward, -flatNor);
				wallBonk = angle < 45f;
			}
			bool ceilingBonk = false;

			if (C.ceiling.collider != null) {
				Vector3 flatNor = C.ceiling.normal;
				flatNor.y = 0;
				float angle = Vector3.Angle(transform.forward, -flatNor);
				ceilingBonk = flatNor.magnitude != 0 && angle < 60f;
			}

			if (ceilingBonk || wallBonk) {
				if (C.speed > 38f) {
					NewState<StateBonk>();
					return true;
				}
				else if (C.speed > 16f) {
					if (alwaysHard)
						NewState<StateBonk>();
					else
						NewState<StateBonkSoft>();
					return true;
				}
				else {
					C.speed = 0;
				}
			}
			return false;
		}

		protected virtual bool LedgeGrabCheck() {
			if (C.vspeed > 0 || wallLastFrame || C.wall.collider == null) {
				wallLastFrame = C.wall.collider != null;
				return false;
			}

			// Look for a floor
			Vector3 to = C.wall.point - C.wall.normal * C.rayCaster.innerRadius;
			RaycastHit floorAbove;
			bool foundFloor = Physics.Raycast(to + Vector3.up * C.rayCaster.height, -Vector3.up,
					out floorAbove, C.rayCaster.height);

			if (!foundFloor) {
				return false;
			}

			// Position Mario above the ledge
			transform.position = floorAbove.point;
			transform.LookAt(transform.position - C.wall.normal);
			NewState<StateGrabLedge>();
			return true;
		}

		protected virtual bool GroundPoundCheck() {
			if (C.input.GetButtonDown("Duck")) {
				NewState<StateGroundPound>();
				return true;
			}
			return false;
		}

		/// <summary>
		/// Called when the physics detect the character entered in contact
		/// with floor.
		/// </summary>
		public virtual void OnLand() {
			NewState<StateLand>();
		}

		public virtual void OnEnteredWater() {
			NewState<StateWaterEnter>();
		}

		public virtual void PlayJumpingSFX() {
			if (previousState != null && previousState is StateGrounded) {
				C.PlayJumpSound();
			}
		}
	}
}
