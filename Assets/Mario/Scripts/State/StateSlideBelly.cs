using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Mario.States {
	public class StateSlideBelly : StateSlideBase {
		public override void Update() {
			if (!IsFloorSteep() &&
					(C.input.GetButtonDown("Attack") || C.input.GetButtonDown("Jump"))) {
				NewState<StateDiveRecover>();
				return;
			}
		}

		public override void FixedUpdate() {
			base.FixedUpdate();

			if (C.speed == 0)
				NewState<StateSlideBellyEnd>();
		}
	}
}
