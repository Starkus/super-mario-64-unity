﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mario.States {
	public class StateBonkSoft : StateAirborne {
		bool canWallJump = true;

		public override void PlayVoiceClip() {
			C.PlayVoiceClip("Ugh");
		}

		public override void Start() {
			if (C.wall.collider != null) {
				Vector3 fw = -Vector3.Reflect(transform.forward, C.wall.normal);
				transform.LookAt(transform.position + fw);
			}

			if (previousState is StateDive || previousState is StateGrabLedge) {
				canWallJump = false;
			}

			C.xspeed = 0;
			if (C.vspeed > 0)
				C.vspeed = 0;

			if (canWallJump && fixedTimer <= 4 && C.input.GetButtonDown("Jump")) {
				NewState<StateWallJump>();
				return;
			}

			C.speed = -8f;
		}

		public override void Update() {
			if (canWallJump && fixedTimer <= 4 && C.input.GetButtonDown("Jump")) {
				NewState<StateWallJump>();
				return;
			}
		}

		public override void FixedUpdate() { }
    }
}
