﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MaterialBank : MonoBehaviour
{
	[System.Serializable]
	public class MaterialDictionary : SerializableDictionary<string, MyPhysicMaterial> { }

	[SerializeField]
	private MaterialDictionary _materialsDictionary;

	public MaterialDictionary materials {
		get {
			return _materialsDictionary;
		}
	}
}
