﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mario.States;

namespace Mario {
	public class BodyRoll : MonoBehaviour {
		Controller C;
		Vector3 initialAngles;
		float currentRoll;
		float currentPitch;

		// Start is called before the first frame update
		void Start()
		{
			C = GetComponentInParent<Controller>();
			initialAngles = transform.localEulerAngles;
		}

		// Update is called once per frame
		void LateUpdate()
		{
			if (!(C.currentState is StateRun)) {
				currentRoll = 0;
				currentPitch = 0;
				return;
			}

			float angle = Vector3.SignedAngle(C.transform.forward, C.input.worldInputVector, -Vector3.up);
			float roll = angle * C.speed;
			roll /= 12f;
			roll = Mathf.Min(30f, Mathf.Max(-30f, roll));

			float delta = 180f * Time.deltaTime;
			
			if (Mathf.Abs(currentRoll - roll) < delta)
				currentRoll = roll;
			else if (currentRoll < roll)
				currentRoll += delta;
			else
				currentRoll -= delta;

			float pitch = C.speed * 0.934f;
			pitch = Mathf.Min(30f, Mathf.Max(0, pitch));

			if (Mathf.Abs(currentPitch - pitch) < delta)
				currentPitch = pitch;
			else if (currentPitch < pitch)
				currentPitch += delta;
			else
				currentPitch -= delta;

			transform.localEulerAngles += new Vector3(currentRoll, currentPitch, 0);
		}
	}
}
