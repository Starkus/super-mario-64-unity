﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyPhysicMaterial : MonoBehaviour
{
	public AudioClip stepSound;
	public AudioClip jumpSound;
	public AudioClip landSound;
}
