using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Mario
{
	public class InputManager : MonoBehaviour
	{
		private Controller C;

		/// <summary>
		/// How far the player has to push the joystick before getting to the
		/// maximum input value. 1.0 is full range.
		/// </summary>
		public float analogRange = 0.66f;

		/// <summary>
		/// What value does the analog tilt reach when at full range.
		/// </summary>
		public float maxTilt = 32f;

		private VirtualPad virtualPad;
		private VirtualJoystick virtualJoystick;

		/// <summary>
		/// Returns the local input vector, X is horizontal and Z is vertical.
		/// </summary>
		public Vector3 inputVector { get { return GetInputVector(); } }
		/// <summary>
		/// What direction in world coordinates the player is pointing to with
		/// their input.
		/// </summary>
		public Vector3 worldInputVector { get { return GetWorldInputVector(); } }
		/// <summary>
		/// The input as seen by the character. For example the Z value returned
		/// is how much we are telling him to move forward, even if both the
		/// character and the joystick are pointing right or any direction.
		/// </summary>
		public Vector3 normalInputVector { get { return GetNormalInputVector(); } }

		public float analogTilt {
			get {
				return inputVector.magnitude;
			}
		}


		private void Start()
		{
			C = GetComponent<Controller>();

			virtualPad = FindObjectOfType<VirtualPad>();

			if (virtualPad != null)
				virtualJoystick = virtualPad.GetComponentInChildren<VirtualJoystick>();
		}

		private Vector3 CircleMap(Vector3 v)
		{
			Vector3 mapped = v;

			if (v.magnitude > 1)
				mapped = v.normalized;

			return mapped;
		}


		private Vector3 GetInputVector()
		{
			Vector3 raw;

			if (virtualPad != null)
			{
				raw = new Vector3(
						Mathf.Clamp(virtualJoystick.horizontal, -analogRange, analogRange) / analogRange,
						0,
						Mathf.Clamp(virtualJoystick.vertical, -analogRange, analogRange) / analogRange
				);
			}
			else
			{
				raw = new Vector3(
						Mathf.Clamp(Input.GetAxis("Horizontal"), -analogRange, analogRange) / analogRange,
						0,
						Mathf.Clamp(Input.GetAxis("Vertical"), -analogRange, analogRange) / analogRange
				);
			}

			Vector3 mapped = CircleMap(raw);

			mapped *= maxTilt;

			return mapped;
		}

		private Vector3 GetWorldInputVector()
		{
			// camera.forward is shifted by framing.
			Vector3 camVector = Vector3.ProjectOnPlane(transform.position - C.activeCamera.transform.position, Vector3.up).normalized;
			Vector3 x = -Vector3.Cross(camVector, Vector3.up).normalized;

			Vector3 target = camVector * inputVector.z;
			target += x * inputVector.x;

			return target;
		}

		private Vector3 GetNormalInputVector()
		{
			Vector3 direction = worldInputVector / 32f;
			Vector3 current = transform.forward;

			Vector3 difference = new Vector3(
						direction.x * current.z - direction.z * current.x,
						0,
						direction.x * current.x + direction.z * current.z);

			difference *= maxTilt;

			return difference;
		}

		/// <summary>
		/// <para>Almost like Input.GetButtonDown but checks <see cref="VirtualPad"/>
		/// if there's any.</para>
		/// <para>Returns true during the frame the user pressed down the virtual button
		/// identified by buttonName.</para>
		/// <para>You may use pipes to OR multiple buttons, ex. "A|X".</para>
		/// </summary>
		/// <param name="s">The name of the button.</param>
		/// <returns>Whether the specified button has been pressed down this frame.
		/// </returns>
		public bool GetButtonDown(string s)
		{
			int pipe = s.IndexOf('|');
			if (pipe != -1) {
				return GetButtonDown(s.Substring(0, pipe)) || GetButtonDown(s.Substring(pipe + 1));
			}
			if (virtualPad == null)
				return Input.GetButtonDown(s);

			return Input.GetButtonDown(s) || virtualPad.GetButtonDown(s);
		}

		/// <summary>
		/// <para>Almost like Input.GetButtonUp but checks <see cref="VirtualPad"/>
		/// if there's any.</para>
		/// <para>Returns true the first frame the user releases the virtual button
		/// identified by buttonName.</para>
		/// <para>You may use pipes to OR multiple buttons, ex. "A|X".</para>
		/// </summary>
		/// <param name="s">The name of the button.</param>
		/// <returns>Whether the specified button has been released this frame.</returns>
		public bool GetButtonUp(string s)
		{
			int pipe = s.IndexOf('|');
			if (pipe != -1) {
				return GetButtonUp(s.Substring(0, pipe)) || GetButtonUp(s.Substring(pipe + 1));
			}
			if (virtualPad == null)
				return Input.GetButtonUp(s);

			return Input.GetButtonUp(s) || virtualPad.GetButtonUp(s);
		}

		public bool GetButton(string s)
		{
			int pipe = s.IndexOf('|');
			if (pipe != -1) {
				return GetButton(s.Substring(0, pipe)) || GetButton(s.Substring(pipe + 1));
			}
			if (virtualPad == null)
				return Input.GetButton(s);

			return Input.GetButton(s) || virtualPad.GetButton(s);
		}
	}
}
